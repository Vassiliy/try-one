FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "user#{n}" }
    password "password"

    factory :admin_user do
      admin true
    end
  end
end
