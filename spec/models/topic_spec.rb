require 'rails_helper'
RSpec.describe Topic, :type => :model do
  it 'is valid with user, title and content' do
    user = FactoryGirl.create :user
    expect(FactoryGirl.build :topic, user: user).to be_valid
  end
  it 'is invalid without user' do
    topic = FactoryGirl.build :topic
    topic.valid?
    expect(topic.errors[:user]).to include("can't be blank")
  end
  it 'is invalid with non-existent user' do
    user = FactoryGirl.create :user
    user_id = user.id
    user.destroy
    topic = Topic.new title: 'MyString', content: 'MyText', user_id: user_id
    topic.valid?
    expect(topic.errors[:user]).to include("can't be blank")
  end
  it 'is invalid without title' do
    topic = FactoryGirl.build :topic, title: nil
    topic.valid?
    expect(topic.errors[:title]).to include("can't be blank")
  end
  it 'is invalid without content' do
    topic = FactoryGirl.build :topic, content: nil
    topic.valid?
    expect(topic.errors[:content]).to include("can't be blank")
  end
  it 'is destroy-dependent to its user' do
    user = FactoryGirl.create(:user)
    5.times { FactoryGirl.create(:topic, user: user) }
    expect { user.destroy }.to change(Topic, :count).by(-5)
  end
  it 'triggers keep_replies on destroy' do
    user = FactoryGirl.create :user
    topic = FactoryGirl.create :topic, user: user
    expect(topic).to receive(:keep_replies)
    topic.destroy
  end
  it 'keeps topic other users posts after deletion' do
    topic_starter = FactoryGirl.create :user
    first_post_author = FactoryGirl.create :user
    second_post_author = FactoryGirl.create :user
    topic = FactoryGirl.create :topic, user: topic_starter
    1.times { FactoryGirl.create(:post, user: topic_starter, topic: topic) }
    2.times { FactoryGirl.create(:post, user: first_post_author, topic: topic) }
    3.times { FactoryGirl.create(:post, user: second_post_author, topic: topic) }
    topic.keep_replies
    expect { topic.destroy }.to change(Post, :count).by(-1)
  end
end

