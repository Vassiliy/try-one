require 'rails_helper'
RSpec.describe User, :type => :model do
  it 'is valid with username and password' do
    expect(FactoryGirl.build :user).to be_valid
  end
  it 'is invalid without username' do
    user = FactoryGirl.build :user, username: nil
    user.valid?
    expect(user.errors[:username]).to include("can't be blank")
  end
  it 'is invalid without password' do
    user = FactoryGirl.build :user, password: nil
    user.valid?
    expect(user.errors[:password]).to include("can't be blank")
  end
  it 'is invalid with a duplicate username' do
    FactoryGirl.create(:user, username: 'cool_user')
    user = FactoryGirl.build(:user, username: 'cool_user')
    user.valid?
    expect(user.errors[:username]).to include('has already been taken')
  end
  describe 'User methods' do
    describe 'topics_count' do
      it 'returns user topics count' do
        user = FactoryGirl.create :user
        5.times { FactoryGirl.create :topic, user: user }
        expect(user.topics_count).to equal(5)
      end
    end
    describe 'posts_count' do
      it 'returns user posts count' do
        user = FactoryGirl.create :user
        topic = FactoryGirl.create :topic, user: user
        5.times { FactoryGirl.create :post, user: user, topic: topic }
        expect(user.posts_count).to equal(5)
      end
    end
  end
end

