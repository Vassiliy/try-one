require 'rails_helper'
RSpec.describe Post, :type => :model do
  let(:topic_starter) { FactoryGirl.create :user }
  let(:topic) { FactoryGirl.create :topic, user: topic_starter }
  
  it 'is valid with user, topic, title and content' do
    post_author = FactoryGirl.create :user
    expect(FactoryGirl.build :post, user: post_author, topic: topic).to be_valid
  end
  it 'is invalid without user' do
    post = FactoryGirl.build :post, topic: topic
    post.valid?
    expect(post.errors[:user]).to include("can't be blank")
  end
  it 'is invalid with non-existent user' do
    user = FactoryGirl.create :user
    user_id = user.id
    user.destroy
    post = Post.new title: 'MyString', content: 'MyText', user_id: user_id, topic_id: topic.id
    post.valid?
    expect(post.errors[:user]).to include("can't be blank")
  end
  it 'is invalid without topic' do
    post = FactoryGirl.build :post, user: topic_starter
    post.valid?
    expect(post.errors[:topic]).to include("can't be blank")
  end
  it 'is invalid with non-existent topic' do
    topic = FactoryGirl.create :topic, user: topic_starter
    topic_id = topic.id
    topic.destroy
    post = Post.new title: 'MyString', content: 'MyText', user_id: topic_starter.id, topic_id: topic_id
    post.valid?
    expect(post.errors[:topic]).to include("can't be blank")
  end
  it 'can be updated without topic' do
    post_author = FactoryGirl.create :user
    post = FactoryGirl.create :post, user: post_author, topic: topic
    post.topic_id = nil
    expect(post).to be_valid
  end
  it 'is invalid without title' do
    post = FactoryGirl.build :post, user: topic_starter, topic: topic, title: nil
    post.valid?
    expect(post.errors[:title]).to include("can't be blank")
  end
  it 'is invalid without content' do
    post = FactoryGirl.build :post, user: topic_starter, topic: topic, content: nil
    post.valid?
    expect(post.errors[:content]).to include("can't be blank")
  end
  it 'is destroy-dependent to its user' do
    post_author = FactoryGirl.create :user
    5.times { FactoryGirl.create(:post, user: post_author, topic: topic) }
    expect { post_author.destroy }.to change(Post, :count).by(-5)
  end
  it 'is destroy-dependent to its (topic starter monologue) topic' do
    doomed_topic = FactoryGirl.create :topic, user: topic_starter
    5.times { FactoryGirl.create(:post, user: topic_starter, topic: doomed_topic) }
    expect { doomed_topic.destroy }.to change(Post, :count).by(-5)
  end
end

