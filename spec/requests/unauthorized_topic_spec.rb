require "rails_helper"

RSpec.describe "Topic actions unauthorized access forbidden:", :type => :request do
  user = FactoryGirl.create :user
  topic = FactoryGirl.create :topic, user: user
  let(:id) { topic.id }
  user.destroy

  it "to POST create" do
    post "/topics", format: :json
    expect(response.code).to eq('401')
  end

  it "to PUT update" do
    put "/topics/#{id}", format: :json
    expect(response.code).to eq('401')
  end

  it "to PATCH update" do
    patch "/topics/#{id}", format: :json
    expect(response.code).to eq('401')
  end

  it "to destroy" do
    delete "/topics/#{id}", format: :json
    expect(response.code).to eq('401')
  end
end
