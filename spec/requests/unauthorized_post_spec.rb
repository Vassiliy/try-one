require "rails_helper"

RSpec.describe "Post actions unauthorized access forbidden:", :type => :request do
  user = FactoryGirl.create :user
  topic = FactoryGirl.create :topic, user: user
  post = FactoryGirl.create :post, user: user, topic: topic
  let(:id) { post.id }
  user.destroy

  it "to POST update" do
    post "/posts", format: :json
    expect(response.code).to eq('401')
  end

  it "to PUT update" do
    put "/posts/#{id}", format: :json
    expect(response.code).to eq('401')
  end

  it "to PATCH update" do
    patch "/posts/#{id}", format: :json
    expect(response.code).to eq('401')
  end

  it "to destroy" do
    delete "/posts/#{id}", format: :json
    expect(response.code).to eq('401')
  end
end
