require "rails_helper"

RSpec.describe "Routing error", :type => :request do
  it "renders 'Not found'" do
    get "/nowhere", format: :json
    expect(response.code).to eq('404')
  end
end
