require "rails_helper"

RSpec.describe "User actions unauthorized access forbidden:", :type => :request do
  user = FactoryGirl.create :user
  let(:id) { user.id }
  user.destroy

  it "to PUT update" do
    put "/users/#{id}", format: :json
    expect(response.code).to eq('401')
  end

  it "to PATCH update" do
    patch "/users/#{id}", format: :json
    expect(response.code).to eq('401')
  end

  it "to destroy" do
    delete "/users/#{id}", format: :json
    expect(response.code).to eq('401')
  end
end
