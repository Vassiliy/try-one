require 'spec_helper'
require 'rails_helper'

RSpec.describe TopicsController, type: :controller do
  let(:user) { FactoryGirl.create :user }
  let(:valid_attributes) { { title: 'MyTitle', content: 'MyContent', user_id: user.id } }
  let(:invalid_attributes) { { title: 'MyTitle', content: nil } }
  
  context "Unauthorized" do
    describe "GET index" do
      context "without params" do
        it "assigns all topics as @topics" do
          topic = Topic.create valid_attributes
          get :index, {}
          expect(assigns(:topics)).to eq([topic])
        end
      end
      context "with params" do
        it "assigns all topics of specified user as @topics" do
          another_user = FactoryGirl.create :user
          topic = Topic.create valid_attributes
          2.times { FactoryGirl.create :topic, user: another_user }
          get :index, { user_id: user.to_param }
          expect(assigns(:topics)).to eq([topic])
        end
      end
    end

    describe "GET show" do
      it "assigns the requested topic as @topic" do
        topic = Topic.create valid_attributes
        get :show, { id: topic.to_param }
        expect(assigns(:topic)).to eq(topic)
      end

      it "renders 'Not found' for non-existent topic" do
        topic = Topic.create valid_attributes
        id = topic.id
        topic.destroy!
        get :show, { id: id }
        expect(response.status).to eq(404)
      end
    end
  end

  context "Authorized as any user" do
    let(:any_user) { FactoryGirl.create :user }
    before do 
      warden.set_user any_user
    end

    describe "POST create" do
      context "with valid params" do
        it "creates a new Topic" do
          expect {
            post :create, { topic: valid_attributes }
          }.to change(Topic, :count).by(1)
          expect(Topic.last.user).to eq(any_user)
        end

        it "assigns a newly created topic as @topic" do
          post :create, { topic: valid_attributes }
          expect(assigns(:topic)).to be_a(Topic)
          expect(assigns(:topic)).to be_persisted
        end
      end

      context "with invalid params" do
        it "assigns a newly created but unsaved topic as @topic" do
          post :create, { topic: invalid_attributes }
          expect(assigns(:topic)).to be_a_new(Topic)
        end
      end
    end

    describe "PUT update with valid (irrelevant) params" do
      let(:new_attributes) { { title: 'NewTitle', content: 'NewContent', user_id: user.id } }

      it "renders 'Not found' for non-existent topic" do
        topic = Topic.create valid_attributes
        id = topic.id
        topic.destroy!
        put :update, { id: id, topic: new_attributes }
        expect(response.status).to eq(404)
      end

      it "forbids editing other users topics" do
        topic = Topic.create valid_attributes
        put :update, { id: topic.to_param, topic: new_attributes }
        expect(response.status).to eq(401)
      end
    end

    describe "DELETE destroy" do
      it "renders 'Not found' for non-existent topic" do
        topic = Topic.create valid_attributes
        id = topic.id
        topic.destroy!
        delete :destroy, { id: id }
        expect(response.status).to eq(404)
      end
      
      it "forbids deleting other users topics" do
        topic = Topic.create valid_attributes
        delete :destroy, { id: topic.to_param }
        expect(response.status).to eq(401)
      end
    end
  end

  context "Authorized as topic owner" do
    before do 
      warden.set_user user
    end

    describe "PUT update" do
      context "with valid params" do
        let(:new_attributes) { { title: 'NewTitle', content: 'NewContent', user_id: user.id } }

        it "allows editing own topics" do
          topic = Topic.create valid_attributes
          put :update, { id: topic.to_param, topic: new_attributes }
          topic.reload
          expect(topic.title).to eq(new_attributes[:title])
        end
      end

      context "with invalid params" do
        it "renders Unprocessable entity" do
          topic = Topic.create valid_attributes
          put :update, { id: topic.to_param, topic: invalid_attributes }
          expect(response.status).to eq(422)
        end
      end
    end

    describe "DELETE destroy" do
      it "renders 'Not found' for non-existent topic" do
        topic = Topic.create valid_attributes
        id = topic.id
        topic.destroy!
        delete :destroy, { id: id }
        expect(response.status).to eq(404)
      end
      
      it "destroys the requested own topic" do
        topic = Topic.create valid_attributes
        expect {
          delete :destroy, { id: topic.to_param }
        }.to change(Topic, :count).by(-1)
      end
      it "but keeps the topic replies" do
        topic = Topic.create valid_attributes
        post_author = FactoryGirl.create :user
        2.times { FactoryGirl.create :post, topic: topic, user: user }
        2.times { FactoryGirl.create :post, topic: topic, user: post_author }
        topic.keep_replies
        expect {
          delete :destroy, { id: topic.to_param }
        }.to change(Post, :count).by(-2)
      end
    end
  end

  context "Authorized as admin" do
    let(:admin) { FactoryGirl.create :admin_user }
    before do 
      warden.set_user admin
    end

    describe "PUT update" do
      let(:new_attributes) { { title: 'NewTitle', content: 'NewContent', user_id: user.id } }

      it "allows editing other users topics" do
        topic = Topic.create valid_attributes
        put :update, { id: topic.to_param, topic: new_attributes }
        topic.reload
        expect(topic.title).to eq(new_attributes[:title])
      end
    end

    describe "DELETE destroy" do
      it "allows deleting other users topics" do
        topic = Topic.create valid_attributes
        expect {
          delete :destroy, { id: topic.to_param }
        }.to change(Topic, :count).by(-1)
      end
      it "deletes topic posts completely" do
        topic = Topic.create valid_attributes
        post_author = FactoryGirl.create :user
        2.times { FactoryGirl.create :post, topic: topic, user: user }
        2.times { FactoryGirl.create :post, topic: topic, user: post_author }
        expect {
          delete :destroy, { id: topic.to_param }
        }.to change(Post, :count).by(-4)
      end
    end
  end
end

