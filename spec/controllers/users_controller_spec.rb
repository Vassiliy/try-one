require 'spec_helper'
require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  let(:valid_attributes) { { username: 'username', password: 'password' } }
  let(:invalid_attributes) { { username: nil, password: 'password' } }

  context "Unauthorized" do
    describe "GET index" do
      context "without params" do
        it "assigns all users as @users" do
          user = User.create! valid_attributes
          get :index, {}
          expect(assigns(:users)).to eq([user])
        end
        it "includes users topics and posts count" do
          user = User.create valid_attributes
          2.times { FactoryGirl.create :topic, user: user }
          topic = FactoryGirl.create :topic, user: user
          5.times { FactoryGirl.create :post, user: user, topic: topic }
          get :index, {}
          json = JSON.parse response.body
          expect(json[0]['topics_count']).to eq(3)
          expect(json[0]['posts_count']).to eq(5)
        end
      end
      
      context "with 'username' param" do
        it "assigns all (eventually 1) user(s) with the requested username as @users" do
          user = User.create valid_attributes
          first_wrong_user = User.create username: 'first', password: 'password'
          second_wrong_user = User.create username: 'second', password: 'password'
          get :index, { username: 'username' }
          expect(assigns(:users)).to eq([user])
        end
      end
    end

    describe "GET show" do
      it "assigns the requested user as @user" do
        user = User.create! valid_attributes
        get :show, { id: user.to_param }
        expect(assigns(:user)).to eq(user)
      end

      it "renders 'Not found' for non-existent user" do
        user = User.create! valid_attributes
        id = user.id
        user.destroy!
        get :show, { id: id }
        expect(response.status).to eq(404)
      end
    end

    describe "POST create" do
      context "with valid params" do
        it "creates a new User" do
          expect {
            post :create, { user: valid_attributes }
          }.to change(User, :count).by(1)
        end

        it "assigns a newly created user as @user" do
          post :create, { user: valid_attributes }
          expect(assigns(:user)).to be_a(User)
          expect(assigns(:user)).to be_persisted
        end
      end

      context "with invalid params" do
        it "assigns a newly created but unsaved user as @user" do
          post :create, { user: invalid_attributes }
          expect(assigns(:user)).to be_a_new(User)
        end
      end
    end
  end

  context "Authorized as user" do
    let(:auth) { FactoryGirl.create :user }
    before do 
      warden.set_user auth
    end

    describe "PUT update" do
      context "with valid params" do
        let(:new_attributes) { { username: 'new_username', password: 'new_password' } }

        it "renders 'Not found' for non-existent user" do
          user = User.create! valid_attributes
          id = user.id
          user.destroy!
          put :update, { id: id, user: new_attributes }
          expect(response.status).to eq(404)
        end

        it "forbids editing other users" do
          user = User.create! valid_attributes
          put :update, { id: user.to_param, user: new_attributes }
          expect(response.status).to eq(401)
        end

        it "updates the currently authorized user" do
          put :update, { id: auth.to_param, user: new_attributes }
          auth.reload
          expect(auth.username).to eq(new_attributes[:username])
        end
      end

      context "with invalid params" do
        it "renders Unprocessable entity" do
          put :update, { id: auth.to_param, user: invalid_attributes }
          expect(response.status).to eq(422)
        end
      end
    end

    describe "DELETE destroy" do
      it "renders 'Not found' for non-existent user" do
        user = User.create! valid_attributes
        id = user.id
        user.destroy!
        delete :destroy, { id: id }
        expect(response.status).to eq(404)
      end
      
      it "destroys the currently authorized user" do
        expect {
          delete :destroy, { id: auth.to_param }
        }.to change(User, :count).by(-1)
      end
    end
  end

  context "Authorized as admin" do
    let(:auth) { FactoryGirl.create :admin_user }
    before do 
      warden.set_user auth
    end

    describe "PUT update" do
      let(:new_attributes) { { username: 'new_username', password: 'new_password' } }

      it "allows editing other users" do
        user = User.create! valid_attributes
        put :update, { id: user.to_param, user: new_attributes }
        user.reload
        expect(user.username).to eq(new_attributes[:username])
      end
    end

    describe "DELETE destroy" do
      it "allows deleting other users" do
        user = User.create! valid_attributes
        expect {
          delete :destroy, { id: user.to_param }
        }.to change(User, :count).by(-1)
      end
    end
  end
end

