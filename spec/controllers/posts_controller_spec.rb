require 'spec_helper'
require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  let(:topic_starter) { FactoryGirl.create :user }
  let(:topic) { FactoryGirl.create :topic, user: topic_starter }
  let(:post_author) { FactoryGirl.create :user }
  let(:valid_attributes) { { title: 'MyTitle', content: 'MyContent', user_id: post_author.id, topic_id: topic.id } }
  let(:invalid_attributes) { { title: 'MyTitle', content: nil } }
  
  context "Unauthorized" do
    describe "GET index" do
      context "without params" do
        it "assigns all posts as @posts" do
          post = Post.create valid_attributes
          get :index, {}
          expect(assigns(:posts)).to eq([post])
        end
      end
      context "with params" do
        it "assigns all posts of specified topic as @posts" do
          post = FactoryGirl.create :post, user: topic_starter, topic: topic
          another_topic = FactoryGirl.create :topic, user: topic_starter
          2.times { FactoryGirl.create :post, user: topic_starter, topic: another_topic }
          get :index, { topic_id: topic.to_param }
          expect(assigns(:posts)).to eq([post])
        end
        it "assigns all posts of specified user as @posts" do
          post = FactoryGirl.create :post, user: topic_starter, topic: topic
          2.times { FactoryGirl.create :post, user: post_author, topic: topic }
          get :index, { user_id: topic_starter.to_param }
          expect(assigns(:posts)).to eq([post])
        end
        it "assigns all posts of specified user in specified topic as @posts" do
          post = FactoryGirl.create :post, user: topic_starter, topic: topic
          another_topic = FactoryGirl.create :topic, user: topic_starter
          2.times { FactoryGirl.create :post, user: post_author, topic: topic }
          2.times { FactoryGirl.create :post, user: topic_starter, topic: another_topic }
          get :index, { user_id: topic_starter.to_param, topic_id: topic.to_param }
          expect(assigns(:posts)).to eq([post])
        end
      end
    end

    describe "GET show" do
      it "assigns the requested post as @post" do
        post = Post.create valid_attributes
        get :show, { id: post.to_param }
        expect(assigns(:post)).to eq(post)
      end

      it "renders 'Not found' for non-existent post" do
        post = Post.create valid_attributes
        id = post.id
        post.destroy!
        get :show, { id: id }
        expect(response.status).to eq(404)
      end
    end
  end

  context "Authorized as any user" do
    let(:any_user) { FactoryGirl.create :user }
    before do 
      warden.set_user any_user
    end

    describe "POST create" do
      context "with valid params" do
        it "creates a new Post" do
          expect {
            post :create, { post: valid_attributes }
          }.to change(Post, :count).by(1)
          expect(Post.last.user).to eq(any_user)
        end

        it "assigns a newly created post as @post" do
          post :create, { post: valid_attributes }
          expect(assigns(:post)).to be_a(Post)
          expect(assigns(:post)).to be_persisted
        end
      end

      context "with invalid params" do
        it "assigns a newly created but unsaved post as @post" do
          post :create, { post: invalid_attributes }
          expect(assigns(:post)).to be_a_new(Post)
        end
      end
    end

    describe "PUT update with valid (irrelevant) params" do
      let(:new_attributes) { { title: 'NewTitle', content: 'NewContent', topic_id: topic.id } }

      it "renders 'Not found' for non-existent post" do
        post = Post.create valid_attributes
        id = post.id
        post.destroy!
        put :update, { id: id, post: new_attributes }
        expect(response.status).to eq(404)
      end

      it "forbids editing other users posts" do
        post = Post.create valid_attributes
        put :update, { id: post.to_param, post: new_attributes }
        expect(response.status).to eq(401)
      end
    end

    describe "DELETE destroy" do
      it "renders 'Not found' for non-existent post" do
        post = Post.create valid_attributes
        id = post.id
        post.destroy!
        delete :destroy, { id: id }
        expect(response.status).to eq(404)
      end
      
      it "forbids deleting other users posts" do
        post = Post.create valid_attributes
        delete :destroy, { id: post.to_param }
        expect(response.status).to eq(401)
      end
    end
  end

  context "Authorized as post owner" do
    before do 
      warden.set_user post_author
    end

    describe "PUT update" do
      context "with valid params" do
        let(:new_attributes) { { title: 'NewTitle', content: 'NewContent', topic_id: topic.id } }

        it "allows editing own posts" do
          post = Post.create valid_attributes
          put :update, { id: post.to_param, post: new_attributes }
          post.reload
          expect(post.title).to eq(new_attributes[:title])
        end
      end

      context "with invalid params" do
        it "renders Unprocessable entity" do
          post = Post.create valid_attributes
          put :update, { id: post.to_param, post: invalid_attributes }
          expect(response.status).to eq(422)
        end
      end
    end

    describe "DELETE destroy" do
      it "renders 'Not found' for non-existent post" do
        post = Post.create valid_attributes
        id = post.id
        post.destroy!
        delete :destroy, { id: id }
        expect(response.status).to eq(404)
      end
      
      it "destroys the requested own post" do
        post = Post.create valid_attributes
        expect {
          delete :destroy, { id: post.to_param }
        }.to change(Post, :count).by(-1)
      end
    end
  end

  context "Authorized as admin" do
    let(:admin) { FactoryGirl.create :admin_user }
    before do 
      warden.set_user admin
    end

    describe "PUT update" do
      let(:new_attributes) { { title: 'NewTitle', content: 'NewContent', topic_id: topic.id } }

      it "allows editing other users posts" do
        post = Post.create valid_attributes
        put :update, { id: post.to_param, post: new_attributes }
        post.reload
        expect(post.title).to eq(new_attributes[:title])
      end
    end

    describe "DELETE destroy" do
      it "allows deleting other users posts" do
        post = Post.create valid_attributes
        expect {
          delete :destroy, { id: post.to_param }
        }.to change(Post, :count).by(-1)
      end
    end
  end
end

