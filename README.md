# Proto forum Rails API
[![Code Climate](https://codeclimate.com/github/vassiliy/try-one/badges/gpa.svg)](https://codeclimate.com/github/vassiliy/try-one)
[![Test Coverage](https://codeclimate.com/github/vassiliy/try-one/badges/coverage.svg)](https://codeclimate.com/github/vassiliy/try-one/coverage)
[![Issue Count](https://codeclimate.com/github/vassiliy/try-one/badges/issue_count.svg)](https://codeclimate.com/github/vassiliy/try-one)
[![Build Status](https://travis-ci.org/vassiliy/try-one.svg?branch=master)](https://travis-ci.org/vassiliy/try-one)

Yes. Forum. People lived on them before social networks and even before LiveJournal. _I met my wife at one forum in 2003_.

Users start topics, post replies to other users topics, guests read, admins make terror, and that is how it goes.

Deploy is as easy as clone, `bundle`, `rake db:create`, `rake db:schema:load`, `rake db:seed`.

Start server by `foreman start` please.

Running test suite is as easy as `rspec`. 

