class PostsController < ApplicationController
  include CommonController

  private
    def allowed_params
      params.require(:post).permit(:title, :content, :topic_id)
    end

    def allowed_filters
      %i[user_id topic_id]
    end
end
