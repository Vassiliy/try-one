module CommonController
  extend ActiveSupport::Concern

  # Contains all CRUD methods for User model associations with the following group permissions:
  # Guests (unauthorized) have read-only access;
  # Users can update and destroy only the records they've created;
  # Admins can create, update and destroy anything and everything.

  # It's a very DRY joke — I merely followed the CodeClimate advice :)

  included do
    prepend_before_filter :set_access_permission, only: %i[update destroy]
    prepend_before_filter :set_record,            only: %i[show update destroy]
    prepend_before_filter :authenticate!,         only: %i[create update destroy]

    def index
      scope = {}
      allowed_filters.each do |filter|
        scope[filter] = params[filter] if params[filter].present?
      end
      all = instance_variable_set "@#{many}", model.where(scope).order(:created_at)
      render json: all.to_json( include: { user: { only: %i[username admin] } } )
    end

    def show
      render json: instance_variable_get("@#{one}")
    end

    def create
      new_record = current_user.public_send(many.to_sym).new(allowed_params)
      new_record = instance_variable_set "@#{one}", new_record

      if new_record.save
        render json: new_record, status: :created, location: new_record
      else
        render json: new_record.errors, status: :unprocessable_entity
      end
    end

    def update
      record = instance_variable_get "@#{one}"
      if record.update(allowed_params)
        head :no_content
      else
        render json: record.errors, status: :unprocessable_entity
      end
    end

    def destroy
      record = instance_variable_get "@#{one}"
      record.destroy
      head :no_content
    end
  end
    
  def set_access_permission
    unless current_user.admin
      if current_user.public_send(many.to_sym).where(id: params[:id]).empty?
        render json: { error: "#{one.capitalize} access violation" }, status: :unauthorized
      end
    end
  end

  def set_record
    instance_variable_set "@#{one}", model.find(params[:id])
  end

  def model
    one.capitalize.constantize
  end

  def one
    many.singularize
  end

  def many
    params[:controller]
  end
end
