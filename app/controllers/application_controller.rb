class ApplicationController < ActionController::API
  include WardenHelper
  
  rescue_from(ActiveRecord::RecordNotFound) { |exception| handle_exception(exception, 404) }

protected

  def handle_exception ex, status
    render json: { error_code: status }, status: status
    logger.error ex   
  end
end

