class UsersController < ApplicationController
  before_filter         :set_access_permission, only: %i[update destroy]
  prepend_before_filter :set_user,              only: %i[show update destroy]
  prepend_before_filter :authenticate!,         only: %i[update destroy]

  def index
    scope = params[:username].present? ? { username: params[:username] } : {}
    @users = User.where scope
    render json: @users.to_json(except: %i[password_digest], methods: %i[topics_count posts_count])
  end

  def show
    render json: @user
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy
    head :no_content
  end

  private

    def set_user
      @user = User.find(params[:id])
    end
    
    def set_access_permission
      unless current_user.admin
        if @user.id != current_user.id
          render json: { error: 'User access violation' }, status: :unauthorized
        end
      end
    end

    def user_params
      params.require(:user).permit(:username, :password)
    end
end

