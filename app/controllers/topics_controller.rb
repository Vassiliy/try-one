class TopicsController < ApplicationController
  include CommonController

  before_filter :destroy_posts, only: %i[destroy]

  private
    def destroy_posts
      @topic.posts.destroy_all if current_user.admin
    end

    def allowed_params
      params.require(:topic).permit(:title, :content)
    end

    def allowed_filters
      %i[user_id]
    end
end

