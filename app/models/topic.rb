class Topic < ActiveRecord::Base
  belongs_to :user

  validates :user, presence: true
  validates :title, presence: true
  validates :content, presence: true

  has_many :posts, dependent: :destroy

  before_destroy :keep_replies

  def keep_replies
    self.posts.where.not(user_id: self.user.id).each { |post| post.update topic_id: nil }
  end 
end
