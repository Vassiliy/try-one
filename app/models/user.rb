class User < ActiveRecord::Base
  has_secure_password
  validates :username, presence: true, uniqueness: true

  has_many :topics, dependent: :destroy
  has_many :posts, dependent: :destroy

  def topics_count
    self.topics.count
  end

  def posts_count
    self.posts.count
  end
end
