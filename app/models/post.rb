class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic

  validates :user, presence: true
  validates :topic, presence: true, on: :create
  validates :title, presence: true
  validates :content, presence: true
end
