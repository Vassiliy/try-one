Rails.application.routes.draw do
  resources :posts, except: [:new, :edit]
  resources :topics, except: [:new, :edit]
  resources :users, except: [:new, :edit]
  root 'topics#index'
  match "*path", to: -> (env) { [404, {}, ['{"error": "not_found"}']] }, via: :all
end

