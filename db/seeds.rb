require 'faker'
user = [ { username: 'nimda', password: 'drowssap', admin: true },
         { username: 'admin', password: 'password', admin: true },
         { username: 'user_one', password: 'one_one' },
         { username: 'user_two', password: 'two_two' } ]

user.each do |record|
  if User.where(username: record[:username]).first.nil?
    user = User.new record
    user.save
  end
end
user_ids = User.all.pluck :id
10.times { Topic.create title: Faker::Lorem.sentence,
                        content: Faker::Lorem.paragraph,
                        user_id: user_ids.sample }
topic_ids = Topic.all.pluck :id
100.times { Post.create title: Faker::Lorem.sentence,
                        content: Faker::Lorem.paragraph,
                        user_id: user_ids.sample,
                        topic_id: topic_ids.sample }

